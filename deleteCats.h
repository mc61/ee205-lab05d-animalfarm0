///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0 - EE 205 - Spr 2022
///
/// This module deletes cats from the database
///
/// @file deleteCats.h
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#ifndef DELETECATS_H
#define DELETECATS_H

#include "catDatabase.h"

void deleteAllCats();

void deleteCat(const size_t index);

#endif