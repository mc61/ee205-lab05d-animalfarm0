///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0 - EE 205 - Spr 2022
///
/// This module updates cats in the database
///
/// @file updateCats.c
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include "updateCats.h"

void updateCatName(const size_t index, const char *newName)
{
    if (nameIsValid(newName) == true)
    {
        strcpy(catNames[index], newName);
    }
}

void fixCat(const size_t index)
{
    catIsFixed[index] = true;
}

void updateCatWeight(const size_t index, const float newWeight)
{
    if (weightIsValid(newWeight) == true)
    {
        catWeights[index] = newWeight;
    }
}