///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0 - EE 205 - Spr 2022
///
/// This module adds cats to the database
///
/// @file addCats.c
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include "addCats.h"

int addCat(const char *name, const enum Gender gender, const enum Breed breed, const bool isFixed, const float weight)
{
    if (
        currentNumberOfCats < MAX_CATS &&
        nameIsValid(name) == true &&
        weightIsValid(weight) == true)
    {
        // addCat parameters are valid
        strcpy(catNames[currentNumberOfCats], name);
        catGenders[currentNumberOfCats] = gender;
        catBreeds[currentNumberOfCats] = breed;
        catIsFixed[currentNumberOfCats] = isFixed;
        catWeights[currentNumberOfCats] = weight;
        currentNumberOfCats++;
        return currentNumberOfCats;
    }
    else
    {
        // addCat input parameters are invalid
        printf("Error: Invalid addCat input\n");
        printf("database has room: %s\nnameIsValid: %s\nweightIsValid: %s\n\n\n",
               currentNumberOfCats < MAX_CATS ? "True" : "False",
               nameIsValid(name) ? "True" : "False",
               weightIsValid(weight) ? "True" : "False");

        return -1; // Error returned
    }
}

bool nameIsValid(const char *name)
{
    // Check to see if name is unique
    bool nameAlreadyExists = false;
    for (size_t i = 0; i < currentNumberOfCats; ++i)
    {
        if (strcmp(catNames[i], name) == 0)
        {
            printf("Error: There already exists a cat in the database with the name %s!\n", name);
            nameAlreadyExists = true;
            continue;
        }
    }
    // name isn't empty, and will fit within the MAX_NAME_LEN
    if (nameAlreadyExists == false && (strlen(name) > 0) && (strlen(name) <= MAX_NAME_LEN - 1))
    {
        return true; // name is valid
    }
    return false; // name is invalid
}

bool weightIsValid(const float weight)
{
    if (weight > 0)
    {
        return true; // weight is valid
    }
    return false; // weight is invalid
}
