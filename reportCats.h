///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0 - EE 205 - Spr 2022
///
/// This module finds cats and prints the entire database
///
/// @file reportCats.h
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#ifndef REPORTCATS_H
#define REPORTCATS_H

#include "catDatabase.h"

void printCat(const size_t index);

void printAllCats();

int findCat(const char *name);

#endif