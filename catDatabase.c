///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0 - EE 205 - Spr 2022
///
/// This module defines all of the enums, the arrays, the maximum size of the array and
/// the number of cats in the array.
///
/// @file catDatabase.h
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include "catDatabase.h"

size_t currentNumberOfCats = 0;
char catNames[MAX_CATS][MAX_NAME_LEN];
enum Gender catGenders[MAX_CATS];
enum Breed catBreeds[MAX_CATS];
bool catIsFixed[MAX_CATS];
float catWeights[MAX_CATS];

void initializeDatabase()
{
    currentNumberOfCats = 0;
    memset(catNames, '\0', sizeof catNames);
    memset(catGenders, UNKNOWN_GENDER, sizeof catGenders);
    memset(catBreeds, UNKNOWN_BREED, sizeof catBreeds);
    // Assume cats are not fixed unless known for sure
    memset(catIsFixed, false, sizeof catIsFixed);
    memset(catWeights, 0, sizeof catWeights);
}