///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0 - EE 205 - Spr 2022
///
/// This module finds cats and prints the entire the database
///
/// @file reportCats.c
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include "reportCats.h"

void printCat(const size_t index)
{
    if (index > currentNumberOfCats)
    {
        printf("animalFarm0: Bad cat [%ld]\n", index);
    }
    else
    {
        printf("cat index = [%lu] name=[%s] gender=[%d] breed=[%d] isFixed=[%d] weight=[%.3f]\n",
               index, catNames[index], catGenders[index], catBreeds[index], catIsFixed[index], catWeights[index]);
    }
}

void printAllCats()
{
    for (size_t index = 0; index < currentNumberOfCats; ++index)
    {
        printCat(index);
    }
}

int findCat(const char *name)
{
    for (size_t i = 0; i < currentNumberOfCats; ++i)
    {
        if (strcmp(name, catNames[i]) == 0)
        {
            return i;
        }
    }
    printf("Error: No cat in the database was found with the name \"%s\"\n", name);
    return -1;
}