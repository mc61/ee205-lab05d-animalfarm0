///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0 - EE 205 - Spr 2022
///
/// Orchestrates the entire program. Start by printing Starting Animal Farm 0 and
/// end by printing Done with Animal Farm 0
///
/// @file main.c
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>

#include "addCats.h"
#include "catDatabase.h"
#include "deleteCats.h"
#include "reportCats.h"
#include "updateCats.h"

int main(void)
{
    printf("Starting Animal Farm 0\n");
    printf("...................................................................\n\n\n");
    printf("MAX_CATS is set to %d\n\n", MAX_CATS);
    printf("MAX_NAME_LEN is set to %d\n\n", MAX_NAME_LEN);

    initializeDatabase();

    printf("Adding a bunch of Cats to the database...\n\n");
    addCat("Loki", MALE, PERSIAN, true, 8.5);
    addCat("Milo", MALE, MANX, true, 7.0);
    addCat("Bella", FEMALE, MAINE_COON, true, 18.2);
    addCat("Kali", FEMALE, SHORTHAIR, false, 9.2);
    addCat("Trin", FEMALE, MANX, true, 12.2);
    addCat("Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0);

    printf("printing all cats that were added to database...\n\n");
    printAllCats();

    printf("\nAttempting to add a cat with a duplicate name...\n");
    addCat("Chili", UNKNOWN_GENDER, SHORTHAIR, false, 19.0);

    printf("Attempting to add a cat with too long of a name, and a negative weight...\n\n");
    addCat("Chiliiiiiiiiiiiiiiiiiiiiiiii30", UNKNOWN_GENDER, SHORTHAIR, false, -19.0);

    printf("Deleting the database entry for the cat \"Loki\"...\n");
    deleteCat(findCat("Loki"));

    printf("\nNow attempting to print the database entry for the deleted cat \"Loki\"...\n\n");
    printCat(findCat("Loki"));

    printf("\n\nAttempting to delete an entry to an index outside the range of the database\n");
    deleteCat(currentNumberOfCats + 1);

    int kali = findCat("Kali");
    printf("\nThis is the database entry for the cat \"%s\" before the changes have been made: \n", catNames[kali]);
    printCat(kali);

    printf("\nAltering the database entry for the cat named \"Kali\"...\n");
    printf("Attempting to set with a duplicate name...\n");
    updateCatName(kali, "Chili"); // this should fail

    printf("\nThis is the database entry for the cat \"%s\" after the failed setName attempt: \n", catNames[kali]);
    printCat(kali);

    printf("\nNow with a valid name and weight, and the cat's going under the knife...\n");
    updateCatName(kali, "Capulet");
    updateCatWeight(kali, 9.9);
    fixCat(kali);
    printCat(kali);

    printf("\nDeleting all cats in database...\n");
    deleteAllCats();
    printf("printing all cats in the database...\n\n");
    printAllCats();

    printf("Done with Animal Farm 0\n");

    return 0;
}
