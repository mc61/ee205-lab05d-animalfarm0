///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0 - EE 205 - Spr 2022
///
/// This module updates cats in the database
///
/// @file updateCats.h
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#ifndef UPDATECATS_H
#define UPDATECATS_H

#include <string.h>

#include "catDatabase.h"
#include "addCats.h"

void updateCatName(const size_t index, const char *newName);

void fixCat(const size_t index);

void updateCatWeight(const size_t index, const float newWeight);

#endif