///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0 - EE 205 - Spr 2022
///
/// This module defines all of the enums, the arrays, the maximum size of the array and
/// the number of cats in the array.
///
/// @file catDatabase.h
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#ifndef CATDATABASE_H
#define CATDATABASE_H

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#define MAX_CATS 6
#define MAX_NAME_LEN 30

extern size_t currentNumberOfCats;

////////////////////////////////////////////////////////////////////////
///////////////////////Data Array Declarations//////////////////////////

extern char catNames[MAX_CATS][MAX_NAME_LEN];

enum Gender
{
    UNKNOWN_GENDER,
    MALE,
    FEMALE
};
extern enum Gender catGenders[];

enum Breed
{
    UNKNOWN_BREED,
    MAINE_COON,
    MANX,
    SHORTHAIR,
    PERSIAN,
    SPHYNX
};
extern enum Breed catBreeds[];

// Assume cats are not fixed unless known for sure
extern bool catIsFixed[];

// Can NOT be negative
extern float catWeights[];

///////////////////End of Data Array Declarations//////////////////////
///////////////////////////////////////////////////////////////////////

void initializeDatabase();

#endif