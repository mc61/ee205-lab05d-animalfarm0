///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0 - EE 205 - Spr 2022
///
/// This module deletes cats from the database
///
/// @file deleteCats.c
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include "deleteCats.h"

void deleteAllCats()
{
    initializeDatabase();
}

void deleteCat(const size_t index)
{
    // validate index to database range
    if (index >= currentNumberOfCats)
    {
        printf("Error: Invalid index input for deleteCat\n");
        return;
    }

    const size_t newNumberOfCats = currentNumberOfCats - 1;

    // Declaring some temp arrays to hold the pre-existing information
    char tempNames[newNumberOfCats][MAX_NAME_LEN];
    enum Gender tempGenders[newNumberOfCats];
    enum Breed tempBreeds[newNumberOfCats];
    bool tempIsFixed[newNumberOfCats];
    float tempWeights[newNumberOfCats];

    // copy over the data from all of the old arrays before the index-to-remove
    for (size_t i = 0; i < index; ++i)
    {
        strcpy(tempNames[i], catNames[i]);
        tempGenders[i] = catGenders[i];
        tempBreeds[i] = catBreeds[i];
        tempIsFixed[i] = catIsFixed[i];
        tempWeights[i] = catWeights[i];
    }
    // copy over the data from all the old arrays after the index-to-remove, covering down on the deleted index
    for (size_t i = index; i < currentNumberOfCats - 1; ++i)
    {
        strcpy(tempNames[i], catNames[i + 1]);
        tempGenders[i] = catGenders[i + 1];
        tempBreeds[i] = catBreeds[i + 1];
        tempIsFixed[i] = catIsFixed[i + 1];
        tempWeights[i] = catWeights[i + 1];
    }

    // reinitialize the old arrays
    initializeDatabase();

    // restore the currentNumberOfCats after reinitialization
    currentNumberOfCats = newNumberOfCats;

    // copy over the old data from temp arrays into the reinitialized arrays
    for (size_t i = 0; i < newNumberOfCats; ++i)
    {
        strcpy(catNames[i], tempNames[i]);
        catGenders[i] = tempGenders[i];
        catBreeds[i] = tempBreeds[i];
        catIsFixed[i] = tempIsFixed[i];
        catWeights[i] = tempWeights[i];
    }
}