///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05d - Animalfarm 0 - EE 205 - Spr 2022
///
/// This module adds cats to the database
///
/// @file addCats.h
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 15_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#ifndef ADDCATS_H
#define ADDCATS_H

#include "catDatabase.h"

int addCat(const char *name, const enum Gender gender, const enum Breed breed, const bool isFixed, const float weight);

bool nameIsValid(const char *name);

bool weightIsValid(const float weight);

#endif